# Odds and Ends

Various bits of code that don't neatly fit into a specific class or project. This includes experiments and fully-operational code.