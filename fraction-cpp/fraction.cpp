#include "fraction.h"

fraction::fraction()
{
    numerator = 1;
    denominator = 1;
}

fraction::fraction(int top, int bottom)
{
    numerator = top;
    denominator = bottom;
}

fraction::fraction(const fraction& frac)
{
    numerator = frac.numerator;
    denominator = frac.denominator;
}
fraction operator+(const fraction& lhs, const fraction& rhs)
{
    fraction sum;
    if (lhs.denominator == rhs.denominator)
    {
        sum.numerator = lhs.numerator + rhs.numerator;
        sum.denominator = lhs.denominator;
    }
    else
    {
        sum.denominator = lhs.denominator * rhs.denominator;
        sum.numerator = (lhs.numerator * rhs.denominator)
            + (rhs.numerator * lhs.denominator);
    }
    return sum;
}
fraction operator+=(fraction& lhs, const fraction& rhs)
{
    lhs = lhs + rhs;
    return lhs; 
}

fraction operator-(const fraction& lhs, const fraction& rhs)
{
    fraction sum;
    if (lhs.denominator == rhs.denominator)
    {
        sum.numerator = lhs.numerator - rhs.numerator;
        sum.denominator = lhs.denominator;
    }
    else
    {
        sum.denominator = lhs.denominator * rhs.denominator;
        sum.numerator = (lhs.numerator * rhs.denominator)
             - (rhs.numerator * lhs.denominator);
    }
    return sum;
}
fraction operator-=(fraction& lhs, const fraction& rhs)
{
    lhs = lhs - rhs;
    return lhs; 
}
fraction operator-(fraction& rhs)
{
    rhs.numerator = -rhs.numerator;
    return rhs;
}
fraction operator*(const fraction& lhs, const fraction& rhs)
{
    fraction product;
    product.numerator = lhs.numerator * rhs.numerator;
    product.denominator = lhs.denominator * rhs.denominator;
    return product;
}
fraction operator*=(fraction& lhs, const fraction& rhs)
{
    lhs = lhs * rhs;
    return lhs;
}
fraction operator/(const fraction& lhs, const fraction& rhs)
{
    fraction product;
    product.numerator = lhs.numerator * rhs.denominator;
    product.denominator = lhs.denominator * rhs.numerator;
    return product;
}
// fraction fraction::exponentiate(fraction a, fraction b)
// {
//     fraction product;
//     product.numerator = a.numerator

// }
// float fraction::toDecimal(fraction frac)
// {
//     float decimal;
//     decimal = frac.numerator / frac.denominator;
//     return decimal;
// }

ostream& operator <<(ostream& outs, const fraction frac)
{
    outs << " " << frac.numerator << "/" << frac.denominator << " ";
    return outs;
}