#include <iostream>

using namespace std;

class fraction
{
    private:
    int numerator;
    int denominator;

    public:
    fraction();
    fraction(int top, int bottom);
    fraction(const fraction& frac);
    friend fraction operator+(const fraction& lhs, const fraction& rhs);
    friend fraction operator+=(fraction& lhs, const fraction& rhs);
    friend fraction operator-(const fraction& lhs, const fraction& rhs);
    friend fraction operator-=(fraction& lhs, const fraction& rhs);
    friend fraction operator-(fraction& rhs);
    friend fraction operator*(const fraction& lhs, const fraction& rhs);
    friend fraction operator*=(fraction& lhs, const fraction& rhs);
    friend fraction operator/(const fraction& lhs, const fraction& rhs);
//    fraction exponentiate(fraction a, fraction b);
//    float toDecimal(fraction frac);
    friend ostream& operator <<(ostream& outs, const fraction frac);

};