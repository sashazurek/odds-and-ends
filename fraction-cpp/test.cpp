#include <iostream>
#include "fraction.h"

using namespace std;

int main()
{
    fraction test(-1,2), test2(-1,4);
    test2 += test;
    cout << test2 << endl;
    test2 -= test;
    cout << test2 << endl;
    test2 *= test;
    cout << test2 << endl;
    return 0;
}