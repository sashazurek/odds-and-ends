﻿using System;
/*
    Challenge #375 from /r/dailyprogrammer
    https://www.reddit.com/r/dailyprogrammer/comments/aq6gfy/20190213_challenge_375_intermediate_a_card/?st=k0h4feae&sh=b303385e
    
 */
namespace cardflip
{
    class game
    {
        static void Main()
        {
            string input;
            char[] deck;

            input = Console.ReadLine();
            deck = input.ToCharArray();
            
            foreach (var element in deck)
                Console.Write(element);

            Console.WriteLine();


        }
    }
}
