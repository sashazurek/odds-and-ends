using System;
namespace HelloWorld
{
    class Hello
    {
        public struct Card
        {
            public int value;
            public string face;
        }
        static void Main()
        {
            System.Console.WriteLine("Testing a struct in C#");
            Card newCard;
            newCard.value = 4;
            newCard.face = "Spades";
            System.Console.WriteLine(newCard.value); 

            // Keep the console window open in debug mode
            System.Console.WriteLine("Press any key to exit.");
            System.Console.ReadKey();
        }
    }
}